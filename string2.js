// ==== String Problem #2 ====
// Given an IP address - "111.139.161.143". Split it into its 
//component parts 111, 139, 161, 143 and return it in an array in numeric values. 
//[111, 139, 161, 143].
// Support IPV4 addresses only. If there are other characters detected, return an empty array.

function IPv4Extractor(value)
{
    if (typeof value === 'string' && IPv4Checker(value))
    {
        const val = value.split('.');
        let output = []
        for (let i = 0 ; i < val.length ; i++) 
        {
            let element = val[i];
            element = 1 * element;
            output.push(element);
        }
        return output;
    }
    else
    {
        return []
    }

}

function IPv4Checker(value)
{
    const val = value.split('.');
    if (val.length != 4)
    {
        return false;
    }
    for (let i = 0 ; i < val.length ; i++) 
    {
        let element = val[i];
        element = 1 * element;
        if (isNaN(element) || element < 0 || element > 255)
        {
            return false;
        }
    }
    return true
}

module.exports = IPv4Extractor
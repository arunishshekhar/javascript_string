const NamePrinter = require("../string4");

console.log(NamePrinter({"first_name": "JoHN", "last_name": "SMith"}))//valid
console.log(NamePrinter({"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}))//valid
console.log(NamePrinter("first_name"))//string input
console.log(NamePrinter())//no input
console.log(NamePrinter([]))//empty array
console.log(NamePrinter(["first_name", "JoHN", "middle_name", "doe", "last_name", "SMith"]))//array
console.log(NamePrinter({}))//empty object
console.log(NamePrinter({"first_name": "first_name", "middle_name": "middle_name", "last_name": "last_name"}))

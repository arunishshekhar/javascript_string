const monthName = require("../string3");

console.log(monthName("10/1/2021")) // Normal
console.log(monthName("pikachu")) // wrong date
console.log(monthName("13/1/2021")) // month as 13 not possible
console.log(monthName("10/32/2021")) // date as 32 not possible
console.log(monthName(32323)) // number insted of string
console.log(monthName()) // empty
console.log(monthName([])) // empty array
console.log(monthName(["10/1/2021"])) // array
console.log(monthName("11/1/2021")) // valid date
console.log(monthName("2/29/2021")) //check for leap year 2021 not a leap year
console.log(monthName("2/29/2020")) // leap year correct
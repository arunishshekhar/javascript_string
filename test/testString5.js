const arrayJoin = require("../string5");

console.log(arrayJoin(["the", "quick", "brown", "fox"]))//normal
console.log(arrayJoin([]))//empty
console.log(arrayJoin())//no value
console.log(arrayJoin("Doremon"))//string
console.log(arrayJoin(646464))//number
console.log(arrayJoin({"first_name": "JoHN", "last_name": "SMith"}))
console.log(arrayJoin({}))//empty object

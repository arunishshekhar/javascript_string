const IPv4Extractor = require('../string2')

console.log(IPv4Extractor("111.139.161.143"))//correct way
console.log(IPv4Extractor(111139161143))//number
console.log(IPv4Extractor())//empty
console.log(IPv4Extractor("111.139.161.143.654"))//wrong IPv4 format
console.log(IPv4Extractor("000.999.161.143"))//Wrong IP value
console.log(IPv4Extractor("Pika Pika"))//Random String
console.log(IPv4Extractor(["111.139.161.143"]))//In array format - wrong input
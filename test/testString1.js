let NumberExtractor = require('../string1')

console.log(NumberExtractor("$100.45"));//normal
console.log(NumberExtractor("100.45"));//wrong format without $
console.log(NumberExtractor("-$100.45"));//normal with -ve
console.log(NumberExtractor(4646));//number
console.log(NumberExtractor());//empty
console.log(NumberExtractor(["$100.45"]));//array
console.log(NumberExtractor([]));//empty array
console.log(NumberExtractor("pika pika"));//random string
console.log(NumberExtractor("$10000.000.00"));//wrong type of number
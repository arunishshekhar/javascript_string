// ==== String Problem #3 ====
// Given a string in the format of "10/1/2021", 
//print the month in which the date is present in. mm/dd/yyyy

function monthName(value)
{
    if (typeof value === 'string' && dateChecker(value))
    {
        const val = value.split('/');
        const month = 1 * val[0];
        const monthName = ['January','February','March','April','May','June',
            'July','August','September','October','November','December']
        return monthName[month-1];
    }
    else
    {
        return "Not a valid Date";
    }
}

function dateChecker(value)
{
    const val = value.split('/');
    const date = 1 * val[1];
    const month = 1 * val[0];
    const year = 1 * val[2];
    if (isNaN(date) || isNaN(month) || isNaN(year))
    {
        return false
    }
    if (month > 12 || month < 1 || !dateCheckerWithMonth(date,month,year))
    {
        return false
    }
    else
    {
        return true;
    }
}

function dateCheckerWithMonth(date,month,year)
{
    let isLeap = false
    if ((year % 4 === 0 && year % 100 != 0) || (year % 100 === 0 && year % 400 == 0))
    {
        isLeap = true
        
    }
    if (date < 0)
    {
        return false
    }
    const leap = [31,29,31,30,31,30,31,31,30,31,30,31];
    const noleap = [31,28,31,30,31,30,31,31,30,31,30,31];
    if (isLeap)
    {
        if (date > leap[month-1])
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    else
    {
        if (date > noleap[month-1])
        {
            return false;
        }
        else
        {
            return true;
        }
    }


}

module.exports = monthName
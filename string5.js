// ==== String Problem #5 ====
// Given an array of strings ["the", "quick", "brown", "fox"], 
//convert it into a string "the quick brown fox."
// If the array is empty, return an empty string.//object

function arrayJoin(value)
{
    if (Array.isArray(value))
    {
        let ans = value.join(' ')
        return ans;
    }
    else
    {
        return "Not a valid Input"
    }
}

module.exports = arrayJoin
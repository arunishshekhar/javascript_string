
// ==== String Problem #4 ====
// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}\ object

function NamePrinter(value)
{
    if (typeof value === 'object' && !Array.isArray(value))
    {
        let name = ''
        for (let i in value)
        {
            let str = value[i].toLowerCase();
            let val = str[0].toUpperCase();
            let ans = val + str.substring(1)
            name = name + " " + ans
        }
        if (name.trim() === '')
        {
            return "Not Valid Object"
        }
        return name.trim()
    }
    else
    {
        return "Not Valid Object"
    }    
}

module.exports = NamePrinter;